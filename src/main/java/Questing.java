import java.util.Stack;

/**
 * 테스트 코드를 보고, 아래 두 method 를 작성해주세요.
 */
class Questing {
	static int quest1(double d)  {
		return (int)d / 2;
	}

	static final String bracketString = "{}[]()";
	static final String bracketOpenString = "{[(";

	static boolean quest2(String s) {

		Stack<Byte> byteStack = new Stack<>();

		for(char c : s.toCharArray()){

			if(bracketString.indexOf(c) == -1) return false;

			if(bracketOpenString.indexOf(c) != -1){
				byteStack.push((byte) c);
				continue;
			}

			byte prevBracket = byteStack.pop();
			// 아스키값으로 체크 닫는 값들은 1 또는 2를 더한값
			if( (byte)c - prevBracket == ((prevBracket < 42) ? 1 : 2) ) continue;

			return false;
		}

		return true;
	}

}
